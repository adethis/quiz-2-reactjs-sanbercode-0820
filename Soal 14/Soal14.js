const volumeBalok = (...plt) => {
  let hasil = 0
  hasil = plt.reduce((prev, curr) => {
    return prev * curr
  })
  return `Volume balok adalah ${hasil}`
}
console.log(volumeBalok(2, 4, 5, 1));

const volumeKubus = (...plt) => {
  let hasil = 0;
  hasil = plt.reduce((prev, curr) => {
    return prev * curr;
  });
  return `Volume Kubus adalah ${hasil}`;
};
console.log(volumeKubus(5, 5, 5))