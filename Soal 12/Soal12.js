class BangunDatar {
  constructor(ss) {
    this.sisi = ss
  }
  get sisi() {
    return this._sisi
  }
  set sisi(s) {
    this._sisi = s
  }

  get luas() {
    return this._sisi * this._sisi
  }
  get keliling() {
    return 4 * this._sisi
  }
}
var bangunDatar = new BangunDatar(5)
console.log(bangunDatar.luas)
console.log(bangunDatar.keliling)



class Lingkaran {
  constructor(jr) {
    this.jari = jr
    this.phi = jr % 7 === 0 ? 22/7 : 3.14
  }
  get jari() {
    return this._jari
  }
  set jari(j) {
    this._jari = j
  }
  get luas() {
    return this.phi * this._jari * this._jari
  }
  get keliling() {
    return 2 * this.phi * this._jari
  }
}
var hitungLingkaran = new Lingkaran(4)
console.log(hitungLingkaran.luas)
console.log(hitungLingkaran.keliling)



class Persegi {
  constructor(ss) {
    this.sisi = ss
  }
  get sisi() {
    return this._sisi
  }
  set sisi(s) {
    this._sisi = s
  }
  get luas() {
    return this._sisi * this._sisi
  }
  get keliling() {
    return 4 * this._sisi
  }
}
var hitungPersegi = new Persegi(5)
console.log(hitungPersegi.luas)
console.log(hitungPersegi.keliling)
