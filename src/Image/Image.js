import React, { Component } from 'react';

class Image extends Component {
  render() {
    return (
      <div className="cardsHeader">
        <img src={this.props.data} alt="img" />
      </div>
    );
  }
}

export default Image