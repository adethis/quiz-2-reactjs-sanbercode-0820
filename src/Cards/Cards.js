import React, { Component } from 'react'
import './Cards.css'
import Image from '../Image/Image'

class Cards extends Component {
  render() {
    const data = this.props.data
    return (
      <div className="sectionCards">
        {
          data.map((item, key) => {
            return (
              <div className="cards" key={key}>
                <Image data={item.photo} />
                <div className="cardsText">
                  <h4>{item.name}</h4>
                  <p>{item.profession}</p>
                  <p>{item.age} years old</p>
                </div>
              </div>
            )
          })
        }
      </div>
    )
  }
}

export default Cards